<div align="center">
  <h3>Hello,</h3> 
  <h1>This is "Abdul Awal Nadim".</h1>
  <h2>Software Engineer || Competitive Coder </h2>
</div>
<div align="center">
  <h3>A passionate Software Developer from Bangladesh.</h3>
</div>

- 👨‍💻 All of my projects are available at -> [Github/aa-nadim](https://github.com/aa-nadim)  ,  [Gitlab/aa-nadim](https://gitlab.com/aa-nadim)

- 👁️ To see my project related videos -> [YouTube/aa nadim](https://www.youtube.com/channel/UC95KDH8V9J4J0AfbLFL1jwg)

- 💬 Ask me about -> **JavaScript, ReactJS, NextJS, NodeJS, MongoDB, ExpressJS.**

- 📫 How to reach me -> **nadim.ice.nstu@gmal.com**

- 📄 Know about my experiences -> [My LinkedIn Profile](https://www.linkedin.com/in/aa-nadim/)


<br>



<div align="center">
 <a href="https://www.linkedin.com/in/aa-nadim/" target="_blank"> <img src="https://localist-images.azureedge.net/photos/35414231625734/big_square/1d3bb99198fc5f10b55f666c09b24b0e1d016199.jpg" alt="LinkedIn" width="60" height="60"/> </a>
<a href="https://www.stopstalk.com/user/profile/Garbage_Value" target="_blank"> <img src="https://avatars.githubusercontent.com/u/14951079?s=200&v=4" alt="stopstalk" width="60" height="60"/> </a>
<a href="https://aa-nadim.web.app/" target="_blank"> <img src="https://i.ibb.co/9vPZb5P/nadim-aa.png" alt="protfolio" width="60" height="60"/> </a>
<a href="https://codeforces.com/profile/GarbageValue" target="_blank"> <img src="https://4.bp.blogspot.com/-v-hzJIq0u7s/WtG1pXclDOI/AAAAAAAABwc/sSKMErfMuecOkdtentny-wBdNTtJi82oQCEwYBhgL/s1600/codeforce.png" alt="codeforces" width="60" height="60"/> </a>
<a href="https://www.hackerrank.com/aa_nadim" target="_blank"> <img src="https://repository-images.githubusercontent.com/231893793/cec60480-04a9-11eb-80c4-df7359d94047" alt="hackerrank" width="60" height="60"/> </a>
</div>

<div style="width: 100%;" align="center">
<img  src="https://github-readme-streak-stats.herokuapp.com/?user=aa-nadim" alt="aa-nadim" />
</div>




<div style="display: flex;">
  <div style="width: 50%;" align="center" >
    
[![Linkedin](https://i.stack.imgur.com/gVE0j.png) LinkedIn](https://www.linkedin.com/in/aa-nadim/)
&nbsp; [![GitHub](https://i.stack.imgur.com/tskMh.png) GitHub](https://github.com/aa-nadim)
 
 <div >
   <div style="width: 50%;" align="center" >
   <a href="https://github.com/antonkomarev/github-profile-views-counter">
    <img src="https://komarev.com/ghpvc/?username=aa-nadim"></a>
  </div>
  </div>
